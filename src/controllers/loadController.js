const express = require('express');
const router = express.Router();
const bcrypt = require('bcrypt');
const {User} = require('../models/userModel');
// const {Truck} = require('../models/truckModel');

const {
  getUsersLoads,
  addLoadForUser,
  getUsersActiveLoads,
  iterateToNextLoadState,
  getUsersLoadById,
  updateUsersLoadById,
  deleteUsersLoadById,
  postUsersLoadById,
  getUsersLoadShippingInfoById,
} = require('../services/loadService');

const {
  asyncAwaitWrapper,
} = require('../utils/apiUtils');
// const truckController = require('./truckController');


// Retrieve the list of loads for authorized user,
// returns list of completed and active loads for Driver and
// list of all available loads for Shipper
// getUsersLoads //

router.get('/', asyncAwaitWrapper( async (req, res) => {
  const {userId} = req.user;
  const status = req.query.status;
  const offset = parseInt(req.query.offset, 10) || 0;
  const limit = parseInt(req.query.limit, 10) || 5;

  const usersLoads = await getUsersLoads(userId, status, offset, limit);

  res.json({
    'loads': usersLoads,
  });
})),


// Add Load for User(abailable only for shipper role)
// Shipper is able to create loads in the system;
// addLoadForUser //

router.post('/', asyncAwaitWrapper(async (req, res) => {
  const {userId} = req.user;
  const data = req.body;
  await addLoadForUser(userId, data);
  res.json({'message': 'Load created successfully'});
}));

// Get user's active load(if exists)
// Retrieve the active load for authorized driver(available only for driver)
// Driver is able to view assigned to him load;
// getUsersActiveLoads

router.get('/active', asyncAwaitWrapper( async (req, res) => {
  const {userId} = req.user;
  const load = await getUsersActiveLoads(userId);
  res.json({load});
}));

// Iterate to next Load state(available only for driver)
// Driver is able to interact with assigned to him load;
// iterateToNextLoadState

router.patch('/active/state', asyncAwaitWrapper(async (req, res) => {
  const {userId} = req.user;
  const newState =await iterateToNextLoadState(userId);
  res.json({'message': `Load state changed to '${newState}'`});
}));

// Get user's Load by id
// getUsersLoadById //

router.get('/:id', asyncAwaitWrapper(async (req, res) => {
  const {userId} = req.user;
  const loadId = req.params.id;
  const load = await getUsersLoadById(loadId, userId);
  res.json({load});
}));

// Update user's load by id (abailable only for shipper role)
// Shipper is able to update loads with status ‘NEW';
// updateUsersLoadById //

router.put('/:id', asyncAwaitWrapper( async (req, res) => {
  const {userId} = req.user;
  const loadId = req.params.id;
  const data = req.body;

  await updateUsersLoadById(loadId, userId, data);

  res.json({'message': 'Load details changed successfully'});
}));

// Delete user's load by id (abailable only for shipper role)
// Delete user's load by id (abailable only for shipper role)
// deleteUsersLoadById //

router.delete('/:id', asyncAwaitWrapper( async (req, res) => {
  const {userId} = req.user;
  const loadId = req.params.id;

  await deleteUsersLoadById(loadId, userId);

  res.json({'message': 'Load deleted successfully'});
}));

// Post a user's load by id, search for drivers (only for shipper role)
// Shipper is able to post a load;
// postUsersLoadById //

router.post('/:id/post', asyncAwaitWrapper( async (req, res) => {
  const {userId} = req.user;
  const loadId = req.params.id;
  const success = await postUsersLoadById(userId, loadId);

  res.json({
    'message': 'Load posted successfully',
    'driver_found': success,
  });
}));

// Get user's Load shipping info by id, returns detailed info about
// shipment for active loads (available only for shipper)
// Shipper is able to view shipping info
// getUsersLoadShippingInfoById //

router.get('/:id/shipping_info', asyncAwaitWrapper( async (req, res) => {
  const {userId} = req.user;
  const loadId = req.params.id;
  const loadInfo = await getUsersLoadShippingInfoById(userId, loadId);
  res.json({loadInfo});
}));

module.exports = {
  loadRout: router,
};
