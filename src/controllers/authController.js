const express = require('express');
const router = express.Router();

const {
  registration,
  logIn,
  restorePassword,
} = require('../services/authService');

const {
  asyncAwaitWrapper,
} = require('../utils/apiUtils');

router.post('/register', asyncAwaitWrapper(async (req, res) => {
  const {email, password, role} = req.body;

  await registration({email, password, role});

  res.json({'message': 'Profile created successfully'});
}));

router.post('/login', asyncAwaitWrapper(async (req, res) => {
  const {
    email,
    password,
  } = req.body;

  const token = await logIn({email, password});
  res.json({'jwt_token': token});
}));

router.post('/forgot_password', asyncAwaitWrapper(async (req, res) => {
  const {email} = req.body;

  await restorePassword({email});
  res.json({'message': 'New password sent to your email address'});
}));

module.exports = {
  authRout: router,
};
