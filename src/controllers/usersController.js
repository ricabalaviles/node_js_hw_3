const express = require('express');
const router = express.Router();
const bcrypt = require('bcrypt');
const {User} = require('../models/userModel');

const {
  getUsersProfileInfo,
  deleteUsersProfile,
  changeUsersPassword,
} = require('../services/usersService');

const {
  asyncAwaitWrapper,
} = require('../utils/apiUtils');

router.get('/', asyncAwaitWrapper(async (req, res) => {
  const {userId} = req.user;
  const user = await getUsersProfileInfo(userId);
  res.json({
    'user': {
      '_id': userId,
      'role': user.role,
      'email': user.email,
      'email': user.email,
      'created_date': user.created_date,
    },
  });
}));

router.delete('/', asyncAwaitWrapper(async (req, res) => {
  const {userId} = req.user;
  await deleteUsersProfile(userId);
  res.json({'message': 'Profile deleted successfully'});
}));

router.patch('/password', asyncAwaitWrapper(async (req, res) => {
  const {
    oldPassword,
    newPassword,
  } = req.body;

  const {userId} = req.user;
  const user = await User.findOne({_id: userId});

  if (!(await bcrypt.compare(oldPassword, user.password))) {
    throw new Error('Invalid  password');
  } else {
    await changeUsersPassword(userId, newPassword);
    res.json({'message': 'Password changed successfully'});
  }
}));


module.exports = {
  usersRout: router,
};
