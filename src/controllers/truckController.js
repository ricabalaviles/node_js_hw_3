const express = require('express');
const router = express.Router();
const bcrypt = require('bcrypt');
const {User} = require('../models/userModel');
// const {Truck} = require('../models/truckModel');

const {
  getUsersTrucks,
  addTruckForUser,
  getUsersTruckById,
  updateUsersTruckById,
  deleteUsersTruckById,
  assignTruckToUserByID,
} = require('../services/trucksService');

const {
  asyncAwaitWrapper,
} = require('../utils/apiUtils');


// Retrieve the list of trucks for authorized user(only for driver role)
router.get('/', asyncAwaitWrapper( async (req, res) => {
  const {userId} = req.user;

  const usersTrucks = await getUsersTrucks(userId);

  res.json({
    'trucks': usersTrucks,
  });
})),


// Add Truck for User(abailable only for driver role)
router.post('/', asyncAwaitWrapper(async (req, res) => {
  const {userId} = req.user;

  const truckType = req.body.type;

  await addTruckForUser(userId, truckType);
  res.json({'message': 'Truck created successfully'});
}));


// Get user's truck by id(abailable only for driver role)
router.get('/:id', asyncAwaitWrapper(async (req, res) => {
  const truckId = req.params.id;
  const {userId} = req.user;
  const truck = await getUsersTruckById(truckId, userId);
  res.json({'truck': {
    '_id': truck._id,
    'created_by': truck.created_by,
    'assigned_to': truck.assigned_to,
    'type': truck.type,
    'status': truck.status,
    'created_date': truck.created_date,
  }});
}));


// Update user's truck by id (abailable only for driver role)
// Driver is able to update not assigned to him trucks info;

router.put('/:id', asyncAwaitWrapper(async (req, res) => {
  const truckId = req.params.id;
  const data = req.body;
  const {userId} = req.user;
  await updateUsersTruckById(truckId, userId, data);

  res.json({'message': 'Truck details changed successfully'});
}));


// Delete user's truck by id (abailable only for driver role)
// Driver is able to delete not assigned to him trucks;
router.delete('/:id', asyncAwaitWrapper(async (req, res) => {
  const {userId} = req.user;
  const truckId = req.params.id;
  await deleteUsersTruckById(truckId, userId);
  res.json({'message': 'Truck deleted successfully'});
}));


// Assign truck to user by id (abailable only for driver role)
// Driver is able to assign truck to himself;
router.post('/:id/assign', asyncAwaitWrapper(async (req, res) => {
  const {userId} = req.user;
  const truckId = req.params.id;
  await assignTruckToUserByID(truckId, userId);
  res.json({'message': 'Truck assigned successfully'});
}));


module.exports = {
  truckRout: router,
};
