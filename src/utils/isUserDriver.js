const {BadRequestError} = require('../utils/errorsUtil');
const {User} = require('../models/userModel');

const isUserDriver = async (userId) => {
  const user = await User.findById(userId);
  if (user.role !== 'DRIVER') {
    throw new BadRequestError('Not permitted. Operation only for "DRIVER"');
  }
};

module.exports = {
  isUserDriver,
};


