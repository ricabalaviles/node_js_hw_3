const checkTruckMatching = (availibleTrucks, load) => {
  const loadSizes = {
    payload: load.payload,
    width: load.dimensions.width,
    length: load.dimensions.length,
    height: load.dimensions.height,
  };
  const sprinterSizes = {
    payload: 1700,
    width: 250,
    length: 300,
    height: 170,
  };
  const smallStraightSizes = {
    payload: 2500,
    width: 250,
    length: 500,
    height: 170,
  };
  const largeStraightSizes = {
    payload: 4000,
    width: 350,
    length: 700,
    height: 200,
  };

  for ( let i = 0; i < availibleTrucks.length; i++) {
    if (availibleTrucks[i].type === 'SPRINTER' &&
     compareSizes(sprinterSizes, loadSizes)) {
      return availibleTrucks[i];
    }

    if (availibleTrucks[i].type === 'SMALL STRAIGHT' &&
     compareSizes(smallStraightSizes, loadSizes)) {
      return availibleTrucks[i];
    }

    if (availibleTrucks[i].type === 'LARGE STRAIGHT' &&
     compareSizes(largeStraightSizes, loadSizes)) {
      return availibleTrucks[i];
    }
  }
};

const compareSizes = (truck, load) => {
  if (truck.payload > load.payload && truck.length > load.length &&
  truck.width > load.width && truck.height > load.height) {
    return truck;
  }
};

module.exports = {
  checkTruckMatching,
};

