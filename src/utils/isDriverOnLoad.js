const {Truck} = require('../models/truckModel');
const {BadRequestError} = require('../utils/errorsUtil');

const isDriverOnLoad = async (userId) => {
  const assignedOlTruck = await Truck.findOne({
    assigned_to: userId,
    status: 'OL',
  });

  if (assignedOlTruck) {
    throw new BadRequestError('DRIVER, keep driving - changes not permitted');
  }
};

module.exports = {
  isDriverOnLoad,
};
