const {BadRequestError} = require('../utils/errorsUtil');
const {User} = require('../models/userModel');

const isUserShipper = async (userId) => {
  const user = await User.findById(userId);
  if (user.role !== 'SHIPPER') {
    throw new BadRequestError('Unavailable operation for this user');
  }
};

module.exports = {
  isUserShipper,
};
