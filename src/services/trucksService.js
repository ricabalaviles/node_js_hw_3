// const {User} = require('../models/userModel');
const {Truck} = require('../models/truckModel');
const {BadRequestError} = require('../utils/errorsUtil');
const {isDriverOnLoad} = require('../utils/isDriverOnLoad');
const {isUserDriver} = require('../utils/isUserDriver');
const trucks = ['SPRINTER', 'SMALL STRAIGHT', 'LARGE STRAIGHT'];


// const bcrypt = require('bcrypt');

const getUsersTrucks = async (userId) => {
  await isUserDriver(userId);

  const usersTrucks = await Truck.find({created_by: userId});

  return usersTrucks;
};

const addTruckForUser = async (userId, truckType) => {
  if (!trucks.some((el) => el === truckType)) {
    throw new BadRequestError('Wrong truck type');
  }

  await isUserDriver(userId);

  await isDriverOnLoad(userId);

  const truck = new Truck({
    created_by: userId,
    type: truckType,
    status: 'IS',
  });

  truck.save();
};

const getUsersTruckById = async (truckId, userId) => {
  await isUserDriver(userId);

  const truck = await Truck.findOne({
    _id: truckId,
    created_by: userId,
  }, '-__v');

  if (!truck) {
    throw new BadRequestError('Nothing found with this id');
  }
  return truck;
};

const updateUsersTruckById = async (truckId, userId, data) => {
  await isUserDriver(userId);

  await isDriverOnLoad(userId);

  const truck = await Truck.findOneAndUpdate({
    _id: truckId,
    created_by: userId,
    assigned_to: {$ne: userId},
  }, {$set: {type: data.type}});

  if (!truck) {
    throw new BadRequestError('Can not find not assigned truck with this id');
  };
};

const deleteUsersTruckById = async (truckId, userId) => {
  await isUserDriver(userId);

  const truck = await Truck.findOneAndDelete({
    _id: truckId,
    created_by: userId,
    assigned_to: {$ne: userId},
  });
  if (!truck) {
    throw new BadRequestError('Can not find not assigned truck with this id');
  };
};

const assignTruckToUserByID = async (truckId, userId) => {
  await isUserDriver(userId);

  const assignedTruck = await Truck.findOne({
    assigned_to: userId,
  });

  if (assignedTruck) {
    throw new BadRequestError('Driver can assign only 1 track');
  }

  const truck = await Truck.findByIdAndUpdate({_id: truckId, created_by: userId}
      , {$set: {assigned_to: userId}});

  if (!truck) {
    throw new BadRequestError('Nothing found with this id');
  };
};

module.exports = {
  getUsersTrucks,
  addTruckForUser,
  getUsersTruckById,
  updateUsersTruckById,
  deleteUsersTruckById,
  assignTruckToUserByID,
};
