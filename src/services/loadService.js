const {User} = require('../models/userModel');
const {Truck} = require('../models/truckModel');
const {Load} = require('../models/loadModel');
const {BadRequestError} = require('../utils/errorsUtil');
const {checkTruckMatching} = require('../utils/checkTruckMatching');
const {isUserShipper} = require('../utils/isUserShipper');

const states = [
  'En route to Pick Up',
  'Arrived to Pick Up',
  'En route to delivery',
  'Arrived to delivery'];
// const statuses = ['NEW', 'POSTED', 'ASSIGNED', 'SHIPPED'];

const getUsersLoads = async (userId, status, offset, limit) => {
  const user = await User.findById(userId);
  if (user.role === 'DRIVER') {
    const usersLoads = await Load.find({
      status: {$ne: 'NEW'},
      assigned_to: userId})
        .skip(offset)
        .limit(limit);
    return usersLoads;
  } else {
    const usersLoads = await Load.find({created_by: userId})
        .skip(offset)
        .limit(limit);
    return usersLoads;
  }
};

const addLoadForUser = async (userId, data) => {
  await isUserShipper(userId);

  const load = new Load({
    'created_by': userId,
    'name': data.name,
    'payload': data.payload,
    'pickup_address': data.pickup_address,
    'delivery_address': data.delivery_address,
    'dimensions': data.dimensions,
  });
  load.save();
};

const getUsersActiveLoads = async (userId) => {
  const load = await Load.findOne({
    assigned_to: userId,
  });
  return load;
};

const iterateToNextLoadState = async (userId) => {
  const load = await Load.findOne({
    assigned_to: userId,
    state: {$ne: null},
  });

  const currentState = load.state;

  if (currentState === 'Arrived to delivery') {
    throw new BadRequestError('This load is shipped already');
  }

  let newState;

  for (let i = 0; i < states.length; i++) {
    if (states[i] === currentState) {
      newState = states[i+1];
    };
  }

  if ( newState === 'Arrived to delivery') {
    await Load.findOneAndUpdate({_id: load._id}, {$set: {status: 'SHIPPED'}});
    await Truck.findOneAndUpdate({assigned_to: load.assigned_to},
        {$set: {status: 'IS'}});
  }

  await Load.findOneAndUpdate({_id: load._id}, {$set: {state: newState}});
  return newState;
};

const getUsersLoadById = async (loadId, userId) => {
  const load = await Load.findOne({
    _id: loadId,
  }, '-__v');

  if (!load) {
    throw new BadRequestError('Nothing found with this id');
  }
  return load;
};

const updateUsersLoadById = async (loadId, userId, data) => {
  await isUserShipper;

  const load = await Load.findOneAndUpdate({
    _id: loadId,
    created_by: userId,
    status: 'NEW',
  }, {$set: {
    name: data.name,
    payload: data.payload,
    pickup_address: data.pickup_address,
    delivery_address: data.delivery_address,
    dimensions: data.dimensions,
  }});

  if (!load) {
    throw new BadRequestError('Can not find "NEW" load with this id');
  };
};

const deleteUsersLoadById = async (loadId, userId) => {
  await isUserShipper;

  const load = await Load.findOneAndDelete({
    _id: loadId,
    created_by: userId,
    status: 'NEW',
  });

  if (!load) {
    throw new BadRequestError('Can not find "NEW" load with this id');
  };
};

const postUsersLoadById = async (userId, loadId) => {
  await isUserShipper(userId);

  const currentUserLoads = await Load.find({created_by: userId});

  if (currentUserLoads.length === 0) {
    throw new BadRequestError('This user dont have any created loads');
  };

  const load = await Load.findOneAndUpdate({
    _id: loadId,
    created_by: userId,
    status: 'NEW',
  }, {$set: {status: 'POSTED'}});

  if (!load) {
    throw new BadRequestError('Can not find "NEW" load with this id');
  };

  const availibleTrucks = await Truck.find({
    status: 'IS',
    assigned_to: {$ne: null},
  });

  const matchingTruck = await checkTruckMatching(availibleTrucks, load);

  //   console.log(matchingTruck);

  const loadLogs = load.logs;

  if (!matchingTruck) {
    // console.log('somthigs goes wrong');
    loadLogs.push({
      message: 'Failed to find driver',
      time: new Date(),
    });

    await Load.findOneAndUpdate({
      _id: loadId,
      created_by: userId,
      status: 'POSTED',
    }, {$set: {status: 'NEW',
      logs: loadLogs}});

    throw new BadRequestError('There is no matchig trucks for this load');
  }

  await Truck.findOneAndUpdate({_id: matchingTruck._id},
      {$set: {status: 'OL'}});

  loadLogs.push({
    message: `Load assigned to driver with id ${matchingTruck.created_by}`,
    time: new Date(),
  });

  await Load.findOneAndUpdate({
    _id: loadId,
    created_by: userId,
    status: 'POSTED',
  }, {$set: {assigned_to: matchingTruck.created_by,
    logs: loadLogs,
    state: 'En route to Pick Up',
    status: 'ASSIGNED',
  }});

  return true;
};

const getUsersLoadShippingInfoById = async (userId, loadId) => {
  await isUserShipper(userId);

  const load = await Load.findOne({
    _id: loadId,
    created_by: userId,
  }, '-__v');

  if (!load) {
    throw new BadRequestError('No active loads with this ID');
  }

  const truck = await Truck.findOne({
    assigned_to: load.assigned_to,
  }, '-__v');
  if (!truck) {
    throw new BadRequestError('No trucks assigned for this ID');
  }

  return {'load': load, 'truck': truck};
};

module.exports = {
  getUsersLoads,
  addLoadForUser,
  getUsersActiveLoads,
  iterateToNextLoadState,
  getUsersLoadById,
  updateUsersLoadById,
  deleteUsersLoadById,
  postUsersLoadById,
  getUsersLoadShippingInfoById,
};
