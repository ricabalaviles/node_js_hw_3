const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');

const {User} = require('../models/userModel');
const {BadRequestError} = require('../utils/errorsUtil');


// Register a new system user(Shipper or Driver)

const registration = async ({email, password, role}) => {
  const user = new User({
    email,
    password: await bcrypt.hash(password, 5),
    role,
  });
  await user.save();
};


// Login into the system, role should be detected automatically

const logIn = async ({email, password}) => {
  const user = await User.findOne({email});

  if (!user) {
    throw new BadRequestError('Invalid email or password');
  }

  if (!(await bcrypt.compare(password, user.password))) {
    throw new BadRequestError('Invalid email or password');
  }

  const token = jwt.sign({
    _id: user._id,
    email: user.email,
  }, 'uberhw');
  return token;
};


// (Optional) Forgot password option
// Any system user can easily reset his password using 'forgot password' option;

const restorePassword = async ({email}) => {
  const user = await User.findOne({email});
  if (!user) {
    throw new BadRequestError('No users with such email');
  } else {
    let newPassword = Math.random().toString(36).slice(-8);
    console.log(newPassword);
    newPassword = await bcrypt.hash(newPassword, 5);

    await User.findOneAndUpdate({email}, {'$set': {'password': newPassword}});
  }
};

module.exports = {
  registration,
  logIn,
  restorePassword,
};
